Name:           perl-DB_File
Version:        1.855
Release:        1
Summary:        Perl5 access to Berkeley DB version 1.x
License:        GPL+ or Artistic
URL:            https://metacpan.org/release/DB_File
Source0:        https://cpan.metacpan.org/authors/id/P/PM/PMQS/DB_File-%{version}.tar.gz
BuildRequires:  coreutils findutils gcc libdb-devel perl-devel perl-generators perl-interpreter perl(Config) perl(ExtUtils::Constant) perl(ExtUtils::MakeMaker)
BuildRequires:  perl(strict) perl(Carp) perl(Exporter) perl(Fcntl) perl(File::Spec) perl(Tie::Hash) perl(warnings) perl(XSLoader)
BuildRequires:  perl(Symbol) perl(Test::More) perl(threads) perl(Test::Pod)
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version)) perl(Fcntl) perl(XSLoader)

%{?perl_default_filter}

%description
DB_File is a module which allows Perl programs to make use of the facilities
provided by Berkeley DB version 1.x (if you have a newer version of DB, you
will be limited to functionality provided by interface of version 1.x). The
interface defined here mirrors the Berkeley DB interface closely.

%package    help
Summary:    Doc files for %{name}
Buildarch:  noarch

%description    help
The %{name}-help package contains doc files for %{name}.

%prep
%setup -q -n DB_File-%{version}
find -type f -exec chmod -x {} +
perl -MConfig -pi -e 's|^#!.*perl|$Config{startperl}|' dbinfo

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 OPTIMIZE="$RPM_OPT_FLAGS"
%{make_build}

%install
make pure_install DESTDIR=%{buildroot}
find '%{buildroot}' -type f -name '*.bs' -exec rm -f {} \;
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes dbinfo README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/DB_File*

%files help
%{_mandir}/*/*

%changelog
* Wed Jan 27 2021 liudabo <liudabo1@huawei.com> - 1.855-1
- upgrade version to 1.855

* Thu Jul 23 2020 xinghe <xinghe1@huawei.com> - 1.853-1
- update version to 1.853

* Thu Sep 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.842-2
- Package init
